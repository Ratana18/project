<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])==0)
{
    header('location:index.php');
}
else{

    if(isset($_POST['add']))
    {
        $location=$_POST['location'];
        $room=$_POST['room'];
        $floor=$_POST['floor'];
        $rack=$_POST['rack'];
        $type=$_POST['type'];
        $filename = $_FILES['file']['name'];
        $file = $_FILES['file']['tmp_name'];
        $destination = 'uploads/' . $filename;
        if (file_exists("uploads/" .$_FILES['file']['name'])){
            $already = $_FILES['file']['name'];
            $_SESSION['already'] = "The File already. ".$filename;
//            header('location:add-doc.php');
        }else{
        $sql="INSERT INTO  tbl_ioc(location,room,floor,rack,type,name) VALUES(:location,:room,:floor,:rack,:type,'$filename')";
        $query = $dbh->prepare($sql);
        $query->bindParam(':location',$location,PDO::PARAM_STR);
        $query->bindParam(':room',$room,PDO::PARAM_STR);
        $query->bindParam(':floor',$floor,PDO::PARAM_STR);
        $query->bindParam(':rack',$rack,PDO::PARAM_STR);
        $query->bindParam(':type',$type,PDO::PARAM_STR);
        $query->execute();


        if (move_uploaded_file($file, $destination)) {
            $lastInsertId = $dbh->lastInsertId();
            if ($lastInsertId) {
                $_SESSION['msg'] = "Document Listed successfully";
                header('location:document.php');
            }
        }else {
                $_SESSION['error'] = "Something went wrong. Please try again";
                header('location:document.php');
            }
        }



    }
    ?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Document Search System | Add New Document</title>
        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONT AWESOME STYLE  -->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLE  -->
        <link href="assets/css/style.css" rel="stylesheet" />
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    </head>
    <body>
    <!------MENU SECTION START-->
    <?php include('includes/header.php');?>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">Add New Document</h4>
            </div>
            <div class="row">
                <?php if($_SESSION['already']!="")
                {?>
                    <div class="col-md-6">
                        <div class="alert alert-warning" >
                            <strong>Hey :</strong>
                            <?php echo htmlentities($_SESSION['already']);?>
                            <?php echo htmlentities($_SESSION['already']="");?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Document Info
                </div>
                <div class="panel-body">
                    <form action="add-doc.php"  role="form" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Location<span style="color:red;">*</span></label>
                            <input class="form-control" type="text" name="location" autocomplete="off"  required />
                        </div>
                        <div class="form-group">
                            <label>Room<span style="color:red;">*</span></label>
                            <input class="form-control" type="text" name="room" autocomplete="off"  required />
                        </div>
                        <div class="form-group">
                            <label>Floor<span style="color:red;">*</span></label>
                            <input class="form-control" type="text" name="floor"  required="required" autocomplete="off"  />
                        </div>
                        <div class="form-group">
                            <label>Rack<span style="color:red;">*</span></label>
                            <input class="form-control" type="text" name="rack" autocomplete="off"   required="required" />
                        </div>

                        <div class="form-group">
                            <label>Document Type<span style="color:red;">*</span></label>
                            <input class="form-control" type="text" name="type" autocomplete="off"   required="required" />
                        </div>

                        <div class="form-group">
                            <label>File<span style="color:red;">*</span></label>
                            <input class="form-control" type="file" name="file" autocomplete="off"   required="required" />
                        </div>
                        <button type="submit" name="add" class="btn btn-info">Add </button>

                    </form>
                </div>
            </div>
        </div>

    </div>

    </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <?php include('includes/footer.php');?>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
    <!-- DATATABLE SCRIPTS  -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
    <!-- CUSTOM SCRIPTS  -->
    <script src="assets/js/custom.js"></script>
    </body>
    </html>
<?php } ?>
