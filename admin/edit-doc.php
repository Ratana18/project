<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])==0)
{
    header('location:index.php');
}
else{
if(isset($_POST['update'])) {
    $location = $_POST['location'];
    $room = $_POST['room'];
    $floor = $_POST['floor'];
    $rack = $_POST['rack'];
    $type = $_POST['type'];
    $filename = $_FILES['file']['name'];
    $file_old = $_POST['file_old'];
    if ($filename != '') {
        $update_filename = $_FILES['file']['name'];
    } else {
        $update_filename = $file_old;
    }
    $id = intval($_GET['id']);
    $sql = "update  tbl_ioc set location=:location,room=:room,floor=:floor,rack=:rack,type=:type,name='$update_filename' where id=:id";
    $query = $dbh->prepare($sql);
    $query->bindParam(':location', $location, PDO::PARAM_STR);
    $query->bindParam(':room', $room, PDO::PARAM_STR);
    $query->bindParam(':floor', $floor, PDO::PARAM_STR);
    $query->bindParam(':rack', $rack, PDO::PARAM_STR);
    $query->bindParam(':type', $type, PDO::PARAM_STR);
    $query->bindParam(':id', $id, PDO::PARAM_STR);
    $query->execute();
    $file = $_FILES['file']['tmp_name'];
        $destination = 'uploads/' . $filename;
    if ($query) {
        if ($_FILES['file']['name'] != '') {
            move_uploaded_file($file, $destination);
            unlink("uploads/" . $file_old);
        }
        $_SESSION['msg'] = "Document info updated successfully 😊";
        header('location:document.php');
    } else {
        $_SESSION['error'] = "Something went wrong. Please try again";
        header('location:document.php');
    }


}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Document Search System | Edit Document</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body>
<!------MENU SECTION START-->
<?php include('includes/header.php');?>
<!-- MENU SECTION END-->
    <div class="content-wrapper">
<div class="container">
    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">Update Document</h4>

        </div>

    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"">
        <div class="panel panel-info">
            <div class="panel-heading">
                Document Info
            </div>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data" >
                    <?php
                    $id=intval($_GET['id']);
                    $sql = "SELECT * from  tbl_ioc  where id=:id";
                    $query = $dbh -> prepare($sql);
                    $query->bindParam(':id',$id,PDO::PARAM_STR);
                    $query->execute();
                    $results=$query->fetchAll(PDO::FETCH_OBJ);
                    $cnt=1;
                    if($query->rowCount() > 0)
                    {
                        foreach($results as $result)
                        {               ?>

                            <div class="form-group">
                                <label>Location<span style="color:red;">*</span></label>
                                <input class="form-control" type="text" name="location" value="<?php echo htmlentities($result->location);?>" required />
                            </div>

                            <div class="form-group">
                                <label>Room<span style="color:red;">*</span></label>
                                <input class="form-control" type="text" name="room" value="<?php echo htmlentities($result->room);?>"  required />
                            </div>

                            <div class="form-group">
                                <label>Floor<span style="color:red;">*</span></label>
                                <input class="form-control" type="text" name="floor" value="<?php echo htmlentities($result->floor);?>"   required />
                            </div>
                            <div class="form-group">
                                <label>Rack<span style="color:red;">*</span></label>
                                <input class="form-control" type="text" name="rack" value="<?php echo htmlentities($result->rack);?>"  required />
                            </div>

                            <div class="form-group">
                                <label>Document Type<span style="color:red;">*</span></label>
                                <input class="form-control" type="text" name="type" value="<?php echo htmlentities($result->type);?>"   required />
                            </div>

                            <div class="form-group">
                                <label>File Name<span style="color:red;">*</span></label>
                                <?php echo htmlentities($result->name);?>
                                <input class="form-control" type="file" name="file" />
                                <input class="form-control" type="hidden" name="file_old" value="<?php echo htmlentities($result->name);?>">
                            </div>

                        <?php }} ?>
                    <button type="submit" name="update" class="btn btn-info">Update </button>

                </form>
            </div>
        </div>
    </div>

</div>

</div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
<?php include('includes/footer.php');?>
<!-- FOOTER SECTION END-->
<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
<!-- CORE JQUERY  -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS  -->
<script src="assets/js/bootstrap.js"></script>
<!-- DATATABLE SCRIPTS  -->
<script src="assets/js/dataTables/jquery.dataTables.js"></script>
<script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
<!-- CUSTOM SCRIPTS  -->
<script src="assets/js/custom.js"></script>
</body>
</html>
<?php } ?>
