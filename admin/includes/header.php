<div class="navbar navbar-inverse set-radius-zero" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" >
                <img src="">
            </a>
        </div>
        <div class="right-div">
            <a href="logout.php" class="btn btn-danger pull-right">LOG OUT <i class="fa fa-sign-out"></i></a>
        </div>
    </div>
</div>

<section class="menu-section">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown" ><i class="fa fa-user"></i> Users <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="add-users.php">Add Users</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="users.php">Manage Users</a></li>
                            </ul>
                        </li>
                        <li><a href="document.php">Document</a></li>
                        <li><a href="add-doc.php">Add New Document</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>
